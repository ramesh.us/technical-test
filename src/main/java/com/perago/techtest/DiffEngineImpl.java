package com.perago.techtest;

import com.perago.techtest.domain.ActionEnum;
import com.perago.techtest.domain.Change;
import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.beanutils.PropertyUtilsBean;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;


public class DiffEngineImpl implements DiffEngine {

    private static final String DOT_OPERATOR = ".";
    private static final String CLASS_PROPERTY_NAME_INDICATOR = "class";

    public <T extends Serializable> T apply(T original, Diff<?> diff) throws DiffException {
        T modified = original;

        List<Change> changes = diff.getChanges();

        PropertyUtilsBean propUtils = new PropertyUtilsBean();

        for(Change change: changes) {
            try {
                if(1 == change.getLevel() && !original.getClass().getSimpleName().equals(change.getPropertyKey())) {
                    propUtils.setProperty(modified, change.getPropertyKey(), change.getNewValue());
                }
            } catch (IllegalAccessException e) {
                throw new DiffException(e);
            } catch (InvocationTargetException e) {
                throw new DiffException(e);
            } catch (NoSuchMethodException e) {
                throw new DiffException(e);
            }
        }
        return modified;
    }

    public <T extends Serializable> Diff calculate(T original, T modified) throws DiffException {
        try {
            String startingPrefix = "1";
            List<Change> changes = new ArrayList<Change>();

            compareObjects(original, modified, startingPrefix, changes, 1);

            Diff diff = new Diff();
            diff.setChanges(changes);
            return diff;

        } catch (IllegalAccessException e) {
            throw new DiffException(e);
        } catch (InvocationTargetException e) {
            throw new DiffException(e);
        } catch (NoSuchMethodException e) {
            throw new DiffException(e);
        }
    }


    private void compareObjects(Object original, Object modified, String prefix, List<Change> changes, int level) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        ActionEnum actionEnum = null;
        String className = null;
        if (original == null && modified == null) {
        } else if (original != null && modified == null) {
            actionEnum = ActionEnum.DELETE;
            className = original.getClass().getSimpleName();
        } else if (original == null && modified != null) {
            actionEnum = ActionEnum.CREATE;
            className = modified.getClass().getSimpleName();
        } else {
            actionEnum = ActionEnum.UPDATE;
            className = original.getClass().getSimpleName();
        }

        int subCount = 0;
        String subPrefix = prefix;
        if (!changes.isEmpty()) {
            subCount++;
            subPrefix = prefix + DOT_OPERATOR + subCount;
        }
        changes.add(new Change(actionEnum, className, null, null, subPrefix, true, level));

        if(!ActionEnum.DELETE.equals(actionEnum)) {
            BeanMap map = new BeanMap(modified);
            PropertyUtilsBean propUtils = new PropertyUtilsBean();

            for (Object key : map.keySet()) {
                String propertyName = (String) key;
                if (original != null) {
                    Object originalProperty = propUtils.getProperty(original, propertyName);
                    Object modifiedProperty = propUtils.getProperty(modified, propertyName);

                    if (modifiedProperty != null && modifiedProperty.getClass().equals(modified.getClass())) {
                        if ((originalProperty == null && modifiedProperty == null)) {
                        } else {
                            if (originalProperty == null && modifiedProperty!=null) {
                                subCount++;
                                changes.add(new Change(actionEnum, propertyName, null, modifiedProperty, prefix + DOT_OPERATOR + subCount, true, level));
                            } else {
                                if (!CLASS_PROPERTY_NAME_INDICATOR.equals(propertyName) || !originalProperty.equals(modifiedProperty)) {
                                    subCount++;
                                    changes.add(new Change(actionEnum, propertyName, null, modifiedProperty, prefix + DOT_OPERATOR + subCount, true, level));
                                }
                            }
                            compareObjects(originalProperty, modifiedProperty, prefix + DOT_OPERATOR + subCount, changes, level + 1);
                        }
                    } else {
                        if (originalProperty == null && !CLASS_PROPERTY_NAME_INDICATOR.equals(propertyName))  {
                            if(modifiedProperty!=null && !ActionEnum.UPDATE.equals(actionEnum)) {
                                subCount++;
                                changes.add(new Change(actionEnum, propertyName, null, modifiedProperty, prefix + DOT_OPERATOR + subCount, false, level));
                            }
                        } else {
                            if (!CLASS_PROPERTY_NAME_INDICATOR.equals(propertyName)) {
                                subCount++;
                                if (originalProperty.getClass().equals(original.getClass())) {
                                    changes.add(new Change(ActionEnum.DELETE, propertyName, null, modifiedProperty, prefix + DOT_OPERATOR + subCount, true, level));
                                } else {
                                    changes.add(new Change(actionEnum, propertyName, originalProperty, modifiedProperty, prefix + DOT_OPERATOR + subCount, false, level));
                                }
                            }
                        }
                    }
                } else {
                    Object modifiedProperty = propUtils.getProperty(modified, propertyName); 
                    if (modifiedProperty != null && modifiedProperty.getClass().equals(modified.getClass())) {
                        subCount++;
                        changes.add(new Change(actionEnum, propertyName, null, null, subPrefix + DOT_OPERATOR + subCount, true, level));
                        compareObjects(null, modifiedProperty, prefix + DOT_OPERATOR + subCount, changes, level + 1);
                    } else if (!CLASS_PROPERTY_NAME_INDICATOR.equals(propertyName)) {
                        subCount++;
                        changes.add(new Change(actionEnum, propertyName, null, modifiedProperty, prefix + DOT_OPERATOR + subCount, false, level));
                    }

                }


            }
        }


    }
}
