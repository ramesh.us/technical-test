package com.perago.techtest.domain;


public enum  ActionEnum {

	CREATE("Create"),
	UPDATE(""),
	DELETE("");

	private String description;

	ActionEnum(String description) {
		this.description = description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
