package com.perago.techtest.domain;


import java.io.Serializable;

public class Change implements Serializable {

    private ActionEnum action;
    private String propertyKey;
    private Object oldValue;
    private Object newValue;
    private String prefix;
    private boolean isClass;
    private int level;

    public Change(ActionEnum action, String propertyKey, Object oldValue, Object newValue, String prefix, boolean isClass, int level) {
        this.action = action;
        this.propertyKey = propertyKey;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.prefix = prefix;
        this.isClass = isClass;
        this.level = level;
    }


    public ActionEnum getAction() {
        return action;
    }

    public void setAction(ActionEnum action) {
        this.action = action;
    }

    public String getPropertyKey() {
        return propertyKey;
    }

    public void setPropertyKey(String propertyKey) {
        this.propertyKey = propertyKey;
    }

    public Object getOldValue() {
        return oldValue;
    }

    public void setOldValue(Object oldValue) {
        this.oldValue = oldValue;
    }

    public String getPrefix() {
        return prefix;
    }

    @Override
    public String toString() {
        return "Change{" +
                "action=" + action +
                ", propertyKey='" + propertyKey + '\'' +
                ", oldValue=" + oldValue +
                ", newValue=" + newValue +
                ", prefix='" + prefix + '\'' +
                ", isClass='" + isClass + '\'' +
                ", level='" + level + '\'' +
                '}';
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Object getNewValue() {
        return newValue;
    }

    public void setNewValue(Object newValue) {
        this.newValue = newValue;
    }

    public void setIsClass(boolean isClass) {
        this.isClass = isClass;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isClass() {
        return isClass;
    }

}
