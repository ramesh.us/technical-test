package com.perago.techtest;

import com.perago.techtest.domain.ActionEnum;
import com.perago.techtest.domain.Change;

import java.util.List;


public class DiffRendererImpl implements DiffRenderer {

    public String render(Diff<?> diff) throws DiffException {

        String renderData = "";

        List<Change> changes = diff.getChanges();
        for (Change change : changes) {
            renderData = display(change, renderData);
        }
        return renderData;
    }

    private String display(Change change, String renderData) {
        ActionEnum action = change.getAction();
        String propertyKey = change.getPropertyKey();
        Object oldValue = change.getOldValue();
        Object newValue = change.getNewValue();

        String prefix = change.getPrefix();

        String dataFormat = "%s\n%s";

        String subData = "";
        if (change.isClass()) {
            subData = String.format("%s%s: %s", prefix, action.name(), propertyKey);
        } else {
            if (action.equals(ActionEnum.CREATE)) {
                subData = String.format("%s%s: %s as %s", prefix, action.name(), propertyKey, newValue);
            } else if (action.equals(ActionEnum.UPDATE)) {
                subData = String.format("%s%s: %s from %s as %s", prefix, action.name(), propertyKey, oldValue, newValue);
            }
        }
        if (renderData.equals("")) {
            renderData = subData;
        } else {
            renderData = String.format(dataFormat, renderData, subData);
        }
        return renderData;
    }
}
