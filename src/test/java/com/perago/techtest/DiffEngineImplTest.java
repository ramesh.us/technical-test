package com.perago.techtest;

import com.perago.techtest.test.Person;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class DiffEngineImplTest {

    @Test
    public void testDiffEngine() throws  DiffException {
        DiffEngineImpl diffEngine = new DiffEngineImpl();

        Person original = DataHelper.createPerson1();
        Person modified = DataHelper.createPerson2();

        Diff diff = diffEngine.calculate(original, modified);

        Person returnedModified = diffEngine.apply(original, diff);
        assertThat(returnedModified, is(modified));
    }

}