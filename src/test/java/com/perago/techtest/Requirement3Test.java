package com.perago.techtest;

import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.perago.techtest.test.Person;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class Requirement3Test {


    private DiffRenderer diffRenderer = new DiffRendererImpl();
    private DiffEngine diffEngine = new DiffEngineImpl();

	
	  @Test public void
	  verifyOriginalNullAndModifiedNotNullObjectsMustReflectAsCreateAction() throws
	  DiffException { 
		  
	Person modified = DataHelper.createPerson1();
	  
	  Diff diff = diffEngine.calculate(null, modified); String render =
	  diffRenderer.render(diff); System.out.println(render);
	  
	  assertThat(render, is(equalTo("1CREATE: Person\n" +
	  "1.1CREATE: firstName as Fred\n" + "1.2CREATE: surname as Smith\n" +
	  "1.3CREATE: friend\n" + "1.3.1CREATE: Person\n" +
	  "1.3.2CREATE: firstName as Tom\n" + "1.3.3CREATE: surname as Brown\n" +
	  "1.3.4CREATE: friend as null\n" + "1.3.5CREATE: nickNames as null\n" +
	  "1.3.6CREATE: pet as null\n"+ "1.4CREATE: nickNames as [carter, jimmy]\n"+
	  "1.5CREATE: pet as Pet{ type=Dog, name=Julie}"))); 
	  }
	  
	  
	  
	  @Test public void
	  verifyOriginalNotNullAndModifiedNullObjectsMustReflectAsDeleteAction() throws
	  DiffException {
	  
	  Person original = DataHelper.createPerson1();
	  
	  Diff diff = diffEngine.calculate(original, null); String render =
	  diffRenderer.render(diff); System.out.println(render);
	  
	  assertThat(render, is(equalTo("1DELETE: Person")));
	  
	  }
	  
	  
	  @Test public void verifyOriginalNotNullAndModifiedNullObjectsMustReflectAsDeleteActionWithSubData
	  () throws DiffException {
		  
	  Person original = DataHelper.createPerson1();
	  Person modified = DataHelper.createPerson1(); modified.setFriend(null);
	  
	  Diff diff = diffEngine.calculate(original, modified); String render =
	  diffRenderer.render(diff); System.out.println(render);
	  
	  assertThat(render, is(equalTo("1UPDATE: Person\n" +
	  "1.1UPDATE: firstName from Fred as Fred\n" +
	  "1.2UPDATE: surname from Smith as Smith\n" + "1.3DELETE: friend\n" +
	  "1.4UPDATE: nickNames from [carter, jimmy] as [carter, jimmy]\n"+
	  "1.5UPDATE: pet from Pet{ type=Dog, name=Julie} as Pet{ type=Dog, name=Julie}"
	  )));
	  
	  }
	 
	  
	  @Test public void voidverifyOriginalNotNullAndModifiedNotNullObjectsMustReflectAsUpdateAction()
	  throws DiffException {
	  
	  Person original = DataHelper.createPerson1(); original.setFriend(null);
	  
	  Person modified = DataHelper.createPerson2();
	  
	  Diff diff = diffEngine.calculate(original, modified); String render =
	  diffRenderer.render(diff); System.out.println(render);
	  
	  assertThat(render, is(equalTo("1UPDATE: Person\n" +
	  "1.1UPDATE: firstName from Fred as Domingos\n" +
	  "1.2UPDATE: surname from Smith as Manuel\n" +
	  "1.3UPDATE: friend\n" +
	  "1.3.1CREATE: Person\n" + 
	  "1.3.2CREATE: firstName as Alex\n" +
	  "1.3.3CREATE: surname as Waller\n" +
	  "1.3.4CREATE: friend as null\n" +
	  "1.3.5CREATE: nickNames as null\n" +
	  "1.3.6CREATE: pet as null\n"+
	  "1.4UPDATE: nickNames from [carter, jimmy] as [carter, Dolly, jimmy]\n"+
	  "1.5UPDATE: pet from Pet{ type=Dog, name=Julie} as Pet{ type=Cat, name=Mo}"))); 
	  
	  }

	 
}
