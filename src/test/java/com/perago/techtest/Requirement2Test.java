package com.perago.techtest;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.perago.techtest.test.Person;

public class Requirement2Test {
	
    private DiffEngine diffEngine = new DiffEngineImpl();
    @Test
    public void verifyPassedParametersAsNotChangedOnCalculate() throws DiffException {
        Person original = DataHelper.createPerson1();
        Person modified = DataHelper.createPerson2();

        Person originalCopy = original;
        Person modifiedCopy = modified;

        assertTrue(original.equals(originalCopy));
        assertTrue(modified.equals(modifiedCopy));
    }

    @Test
    public void verifyPassedParametersAsNotChangedOnApply() throws DiffException, NoSuchFieldException {
        Person original = DataHelper.createPerson1();
        Person modified = DataHelper.createPerson2();
        Person originalCopy = original;

        Diff diff = diffEngine.calculate(original, modified);
        diffEngine.apply(originalCopy, diff);

        assertTrue(original.equals(originalCopy));
    }
}
