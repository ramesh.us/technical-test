package com.perago.techtest;


import com.perago.techtest.test.Person;
import com.perago.techtest.test.Pet;

import java.util.HashSet;
import java.util.Set;

public class DataHelper {

    public static Person createPerson1() {
        Set nickNamesSet1 = getNickNames();

        Person subPerson1 = new Person();
        subPerson1.setFirstName("Tom");
        subPerson1.setSurname("Brown");
        
        Pet pet1 = new Pet();
        pet1.setType("Dog");
        pet1.setName("Julie");

        Person person1 = new Person();
        person1.setFirstName("Fred");
        person1.setSurname("Smith");
        person1.setNickNames(nickNamesSet1);
        person1.setFriend(subPerson1);
        person1.setPet(pet1);

        return person1;
    }

    public static Person createPerson2() {
        Set nickNamesSet1 = getNickNames();

        Set nickNamesSet2 = new HashSet();
        nickNamesSet2.addAll(nickNamesSet1);

        Person subPerson2 = new Person();
        subPerson2.setFirstName("Alex");
        subPerson2.setSurname("Waller");
        
        Pet pet2 = new Pet();
        pet2.setType("Cat");
        pet2.setName("Mo");

        Person person2 = new Person();
        person2.setFirstName("Domingos");
        person2.setSurname("Manuel");
        nickNamesSet2.add("Dolly");
        person2.setNickNames(nickNamesSet2);
        person2.setFriend(subPerson2);
        person2.setPet(pet2);
        return person2;
    }

    private static Set getNickNames() {
        Set nickNamesSet1 = new HashSet();
        nickNamesSet1.add("jimmy");
        nickNamesSet1.add("carter");
        return nickNamesSet1;
    }
}
