package com.perago.techtest;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.perago.techtest.test.Person;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class Requirement5Test {
	
	private DiffEngine diffEngine = new DiffEngineImpl();
    private DiffRenderer diffRenderer = new DiffRendererImpl();

    @Test
    public void testVerifyDiffReflectsRecursively() throws DiffException {

        Person original = DataHelper.createPerson1();

        Person modified = DataHelper.createPerson2();

        Set nickNames = new HashSet<String>();
        nickNames.add("Sisi");

        Person subFriend = new Person();
        subFriend.setFirstName("Dummny");
        subFriend.setSurname("Jean");
        subFriend.setNickNames(nickNames);

        modified.getFriend().setFriend(subFriend);

        Diff diff = diffEngine.calculate(original, modified);
        String render = diffRenderer.render(diff);
        System.out.println(render);

        assertThat(render, is(equalTo("1UPDATE: Person\n" +
                "1.1UPDATE: firstName from Fred as Domingos\n" +
                "1.2UPDATE: surname from Smith as Manuel\n" +
                "1.3UPDATE: friend\n" +
                "1.3.1UPDATE: Person\n" +
                "1.3.2UPDATE: firstName from Tom as Alex\n" +
                "1.3.3UPDATE: surname from Brown as Waller\n" +
                "1.3.4UPDATE: friend\n" +
                "1.3.4.1CREATE: Person\n" +
                "1.3.4.2CREATE: firstName as Dummny\n" +
                "1.3.4.3CREATE: surname as Jean\n" +
                "1.3.4.4CREATE: friend as null\n" +
                "1.3.4.5CREATE: nickNames as [Sisi]\n" +
                "1.3.4.6CREATE: pet as null\n"+
                "1.4UPDATE: nickNames from [carter, jimmy] as [carter, Dolly, jimmy]\n"+
                "1.5UPDATE: pet from Pet{ type=Dog, name=Julie} as Pet{ type=Cat, name=Mo}")));
    }
}
