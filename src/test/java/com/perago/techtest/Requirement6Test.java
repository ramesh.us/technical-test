package com.perago.techtest;

import org.junit.Test;

import com.perago.techtest.test.Person;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class Requirement6Test {
	
	private DiffEngine diffEngine = new DiffEngineImpl();
    private DiffRenderer diffRenderer = new DiffRendererImpl();

    @Test
    public void testChangesOnCollection() throws DiffException {

        Person original = DataHelper.createPerson1();

        Person modified = DataHelper.createPerson2();

        Diff diff = diffEngine.calculate(original, modified);
        String render = diffRenderer.render(diff);
        System.out.println(render);

        assertThat(render, is(equalTo("1UPDATE: Person\n" +
                "1.1UPDATE: firstName from Fred as Domingos\n" +
                "1.2UPDATE: surname from Smith as Manuel\n" +
                "1.3UPDATE: friend\n" +
                "1.3.1UPDATE: Person\n" +
                "1.3.2UPDATE: firstName from Tom as Alex\n" +
                "1.3.3UPDATE: surname from Brown as Waller\n" +
                "1.4UPDATE: nickNames from [carter, jimmy] as [carter, Dolly, jimmy]\n"+
                "1.5UPDATE: pet from Pet{ type=Dog, name=Julie} as Pet{ type=Cat, name=Mo}")));
    }

}
