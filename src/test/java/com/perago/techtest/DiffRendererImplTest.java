package com.perago.techtest;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import com.perago.techtest.test.Person;
import com.perago.techtest.test.Pet;

public class DiffRendererImplTest {

	private static final String EXPECTED_RENDERING_DATA = "1UPDATE: Person\n"
			+ "1.1UPDATE: firstName from Fred as Samuel\n" + "1.2UPDATE: surname from Smith as Sachin\n"
			+ "1.3UPDATE: friend\n" + "1.3.1UPDATE: Person\n" + "1.3.2UPDATE: surname from Patel as Naido\n"
			+ "1.4UPDATE: nickNames from [bajia, aloo] as [bajia, doll, aloo]\n"
			+"1.5UPDATE: pet from Pet{ type=null, name=Julie} as Pet{ type=null, name=Mew}";

	@Test
	public void testRenderer() throws DiffException {

		DiffEngineImpl diffEngine = new DiffEngineImpl();
		Set nickNamesSet1 = new HashSet();
		nickNamesSet1.add("aloo");
		nickNamesSet1.add("bajia");

		Set nickNamesSet2 = new HashSet();
		nickNamesSet2.addAll(nickNamesSet1);

		Person subPerson1 = new Person();
		subPerson1.setSurname("Patel");
		
		Pet pet1 = new Pet();
		pet1.setName("Julie");

		Person subPerson2 = new Person();
		subPerson2.setSurname("Naido");
		
		Pet pet2 = new Pet();
		pet2.setName("Mew");

		Person person1 = new Person();
		person1.setFirstName("Fred");
		person1.setSurname("Smith");
		person1.setNickNames(nickNamesSet1);
		person1.setFriend(subPerson1);
		person1.setPet(pet1);

		Person person2 = new Person();
		person2.setFirstName("Samuel");
		person2.setSurname("Sachin");
		nickNamesSet2.add("doll");
		person2.setNickNames(nickNamesSet2);
		person2.setFriend(subPerson2);
		person2.setPet(pet2);

		Diff diff = diffEngine.calculate(person1, person2);

		DiffRendererImpl diffRenderer = new DiffRendererImpl();
		String render = diffRenderer.render(diff);
		
		assertThat(render, is(EXPECTED_RENDERING_DATA));
	}
}
